# Motion detection

## Ubuntu

### Dependencies
* **opencv >= 4.2**

    Install: `sudo apt install libopencv-dev`<br>
    In some cases, you may need to move the opencv2 folder from`/usr/include/opencv4/` to `/usr/include/`.<br>
    Use: `sudo mv /usr/include/opencv4/opencv2/ /usr/include/`

* **cmake >= 3.16**

### Build
* Clone the project
* Go in the project folder and create a `build` directory
* Go in the `build` directory and use: `cmake ..`
* Make: `make -j`

### Run
* You will find all the different applications in the `build` directory
* Then, run the one you want, example `./build/src/basic_model/basic_model ...args`

## Windows

### Dependencies
* **opencv >= 4.2**

    Download: https://opencv.org/releases/ <br>
    Add an `OPENCV_DIR` environment variable pointing to `...\opencv\build\x64\vc15` <br>
    Then, in the `Path` environment variable, add `%OPENCV_DIR%\bin` and `%OPENCV_DIR%\lib`
    
* **cmake >= 3.16**

    Download: https://cmake.org/download/

### Build
* In MVS:

    Open the project as a `CMake project`. <br>
    You may need to set some settings (add `CMakeSettings.json` at the root). You can use the following:
    ```json
        {
          "configurations": [
            {
              "name": "x64-Debug",
              "generator": "Ninja",
              "configurationType": "Debug",
              "inheritEnvironments": [ "msvc_x64_x64" ],
              "buildRoot": "${projectDir}\\out\\build\\${name}",
              "installRoot": "${projectDir}\\out\\install\\${name}",
              "cmakeCommandArgs": "",
              "buildCommandArgs": "",
              "ctestCommandArgs": ""
            },
            {
              "name": "x64-Release",
              "generator": "Ninja",
              "configurationType": "RelWithDebInfo",
              "buildRoot": "${projectDir}\\out\\build\\${name}",
              "installRoot": "${projectDir}\\out\\install\\${name}",
              "cmakeCommandArgs": "",
              "buildCommandArgs": "",
              "ctestCommandArgs": "",
              "inheritEnvironments": [ "msvc_x64_x64" ],
              "variables": []
            }
          ]
        }
    ```
    Then, click on `build` (right click on the `CMakeLists.txt`)

### Run
To enter command line arguments, you can add this configuration to the `.vs/launch.json` file:
```json
    {
      "type": "default",
      "project": "CMakeLists.txt",
      "projectTarget": "basic_model.exe (src\\basic_model\\basic_model.exe)",
      "name": "basic_model.exe (src\\basic_model\\basic_model.exe)",
      "args": [ "-f", "first_arg", "-s", "second_arg" ]
    }
```
Then, choose the application to run from the debug menu (*green play button*)