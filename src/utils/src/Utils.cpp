#include "../include/Utils.hpp"

/// Parse command line arguments
/// This is surely not the best way to do it, but I wanted to try to build my own command line parser
std::map<std::string, std::pair<bool, std::string>> parse_command_line_args(const int& argc, char* const argv[], const std::vector<std::string>& options) {
	std::map<std::string, std::pair<bool, std::string>> result;
	char tmp_buffer[256];
	size_t id = 0;

	// Extract data from argv
	std::vector<std::string> data;
	for (int argv_id = 1; argv_id < argc; ++argv_id) {
		#ifdef _WIN32
		sscanf_s(argv[argv_id], "%[^\n]", tmp_buffer, 256);
		#endif
		#ifdef linux
		sscanf(argv[argv_id], "%[^\n]", tmp_buffer);
		#endif
		data.push_back(std::string(tmp_buffer));
	}

	// Iterate through all the given options and find if the user provided it
	for (auto& option: options) {
		auto iterator = std::find(data.begin(), data.end(), option);
		if (iterator != data.end()) {
			id = static_cast<size_t>(std::distance(data.begin(), iterator));
			if (++id <= data.size() - 1) {
				// Verify that the argument is not the next option
				if (std::find(options.begin(), options.end(), data[id]) == options.end()) {
					result.insert({ option, {true, data[id]} });
				} else {
					result.insert({ option, {true, ""} });
				}
			} else {
				result.insert({ option, {true, ""} });
			}
		} else {
			result.insert({ option, {false, ""} });
		}
	}

	return result;
}


/// Update the value of a CameraSetting
void CameraSetting::update(const int& key_pressed) {
	if (editable) {
		if (key_pressed == inc_trigger) {
			if (value < max) { value += step; }
			if (value >= max) { value = max; }
		}
		else if (key_pressed == dec_trigger) {
			if (value > min) { value -= step; }
			if (value <= min) { value = min; }
		}
	}
}


/// Initialize the camera setting
void CameraSetting::initialize(cv::VideoCapture& video) {
	editable = video.set(prop_id, value);
	if (!editable) std::cerr << "Warning : '" << name << "' setting is not editable." << std::endl;
}


/// Check if the camera trigger is being triggered
bool CameraSetting::is_triggered(const int& key_pressed) {
	return key_pressed == inc_trigger || key_pressed == dec_trigger;
}


/// Custom '<<' operator for the CameraSetting struct
std::ostream& operator<<(std::ostream& os, const CameraSetting& camera_setting) {
	if (camera_setting.editable) {
		os << camera_setting.name << ": "
			<< ((camera_setting.editable) ? "editable" : "non editable") << ", "
			<< "(virtual)value = " << camera_setting.value << ", "
			<< "step = " << camera_setting.step << ", "
			<< "min = " << camera_setting.min << ", "
			<< "max = " << camera_setting.max;
	}
	else {
		os << camera_setting.name << ": " << "not editable.";
	}

	return os;
}


/// Compute the median of the given list of integers
int compute_median(const std::vector<int>& values) {
	// Sort the list of integers so that all the values before the middle are less or equal to the middle value.
	// Likewise, all the values after the middle are greater than the middle value.
	auto integers(values);
	auto middle = integers.begin() + integers.size() / 2;
	std::nth_element(integers.begin(), middle, integers.end());
	return integers[integers.size() / 2];
}


/// Compute the mean of the given list of integers
float compute_mean(const std::vector<int>& values) {
	return static_cast<float>(std::accumulate(values.begin(), values.end(), 0)) / static_cast<float>(values.size());
}


/// Compute the mean of the given list of floats
float compute_mean(const std::vector<float>& values) {
	return std::accumulate(values.begin(), values.end(), 0) / static_cast<float>(values.size());
}


/// Compute the standard deviation of the given list of integers and its mean
float compute_standard_deviation(const std::vector<int>& values, const float& mean) {
	std::vector<float> squared_deviations(values.begin(), values.end());
	std::for_each(squared_deviations.begin(), squared_deviations.end(), [mean](float& value) -> void {
		value = value - mean;
		value *= value;
	});
	float variance = compute_mean(squared_deviations);
	return std::sqrt(variance);
}


/// Update method of a Gaussian
void Gaussian::update_mu_sigma(const float& value, const float& alpha) {
	const float rho = alpha * compute_normal_pdf<float>(value, m_mu, m_sigma);
	m_mu = (1.0f - rho) * m_mu + rho * value;
	m_sigma = std::sqrt((1.0f - rho) * (m_sigma * m_sigma) + rho * (value - m_mu) * (value - m_mu));
}

void Gaussian::update_weight(const float& alpha, const bool& matched) {
	if (matched) {
		m_weight = (1.0f - alpha) * m_weight + alpha;
	}
	else {
		m_weight = (1.0f - alpha) * m_weight;
	}
}


/// Custom '<<' operator for the Gaussian struct
std::ostream& operator<<(std::ostream& os, const Gaussian& gaussian) {
	os	<< "Gaussian: "
		<< "weight = " << gaussian.m_weight << ", "
		<< "mu = " << gaussian.m_mu << ", "
		<< "sigma = " << gaussian.m_sigma;

	return os;
}