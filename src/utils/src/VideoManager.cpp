#include "../include/VideoManager.hpp"


/// Initialization of the video manager
bool VideoManager::initialize(const std::string& file_path) {
    // Initialize the video capture
    const bool cameraInit = set_video(file_path);
    // Stop the programm if there is an error opening the video flux
    if (!cameraInit) {
        std::cerr << "VideoManager::initialize: error opening the video file." << std::endl;
        return false;
    }

    m_width = m_video.get(cv::CAP_PROP_FRAME_WIDTH);
    m_height = m_video.get(cv::CAP_PROP_FRAME_HEIGHT);
    m_length = m_video.get(cv::CAP_PROP_FRAME_COUNT);
    m_fps = m_video.get(cv::CAP_PROP_FPS);

    return true;
}


/// Set a new video capture
bool VideoManager::set_video(const std::string& file_path) {
    m_video.release();
    m_video = cv::VideoCapture(file_path);
    return m_video.isOpened();
}


/// Manage all the events of the VideoManager
void VideoManager::manage_events(const int& key_pressed) {
    switch (key_pressed)
    {
        default:
            break;
    }
}