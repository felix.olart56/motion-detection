#include "../include/CameraManager.hpp"
#include "../include/App.hpp"

/// Initialization of the camera manager (settings, video, etc)
bool CameraManager::initialize(const int& index, const std::string& file_path, const int& api_preference) {
    // Import camera settings
    json settings_file;
    std::ifstream json_file(file_path);
    if (json_file) {
        try {
            settings_file = json::parse(json_file, /* callback */ nullptr, /* allow exceptions */ true, /* ignore comments */ true);
        }
        catch (const std::exception& exception) {
            std::cerr << "CameraManager::initialize: import camera settings, " << exception.what() << std::endl;
            return false;
        }
    }
    else {
        std::cerr << "CameraManager::initialize: unable to open '" << file_path << "'." << std::endl;
        return false;
    }

    // Initialize the video capture
    const bool cameraInit = set_video(index, api_preference);
    // Stop the programm if there is an error opening the video flux
    if(!cameraInit) {
        std::cerr << "CameraManager::initialize: error opening the video flux." << std::endl;
        return false;
    }

    // Setup camera settings (needs video to be setup)
    try {
        setup_settings(settings_file);
    }
    catch (const std::exception& exception) {
        std::cerr << "CameraManager::initialize: setup camera settings, " << exception.what() << std::endl;
        return false;
    }

    // Show settings information
    print_settings();

    return true;
}


/// Read data from the json settings file and setup the corresponding parameters
void CameraManager::setup_settings(json file) {
    for (auto& setting : file) {
        CameraSetting cam_setting = {
            setting["name"],
            setting["prop_id"],
            true,
            setting["default_value"],
            setting["step"],
            setting["min"],
            setting["max"],
            setting["dec_trigger"],
            setting["inc_trigger"]
        };
        cam_setting.initialize(m_video);
        m_settings.push_back(cam_setting);
    }
}


/// Set a new video capture
bool CameraManager::set_video(const int& index, const int& api_preference) {
    m_video.release();
    m_video = cv::VideoCapture(index, api_preference);
    return m_video.isOpened();
}


/// Print useful information on settings
void CameraManager::print_settings() const {
    std::cout << "-- Camera settings ------------------------------" << std::endl;
    std::cout << "Backend name: " << m_video.getBackendName() << std::endl;
    for (auto& setting : m_settings) {
        if (setting.editable) {
            std::cout << setting << ", (real)value = " << m_video.get(setting.prop_id) << std::endl;
        }
        else {
            std::cout << setting << std::endl;
        }
    }
}


/// Manage all the events of the CameraManager
void CameraManager::manage_events(const int& key_pressed) {
    // std::cout << key_pressed << std::endl;

    // Update settings
    for (auto& setting : m_settings) {
        if (setting.is_triggered(key_pressed)) {
            setting.update(key_pressed);
            if (setting.editable) {
                m_video.set(setting.prop_id, setting.value);
            }
        }
    }

    // Other keys
    switch (key_pressed)
    {
        case App::Key::S:
            print_settings();
            break;

        default:
            break;
    }
}