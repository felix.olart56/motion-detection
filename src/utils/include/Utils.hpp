#pragma once

#ifdef _WIN32
#pragma warning(push, 0)
#endif
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <limits>
#include <numeric>
#include <omp.h>
#define _USE_MATH_DEFINES
#include <math.h>
#ifdef _WIN32
#pragma warning(pop)
#endif

struct CameraSetting {
    std::string name;
    cv::VideoCaptureProperties prop_id;
    bool editable;
    double value;
    double step;
    int min;
    int max;
    int dec_trigger;
    int inc_trigger;

    void initialize(cv::VideoCapture& video);
    bool is_triggered(const int& key_pressed);
    void update(const int& key_pressed);
};

std::map<std::string, std::pair<bool, std::string>> parse_command_line_args(const int& argc, char* const argv[], const std::vector<std::string>& options);

std::ostream& operator<<(std::ostream& os, const CameraSetting& camera_setting);

int compute_median(const std::vector<int>& values);
float compute_mean(const std::vector<int>& values);
float compute_mean(const std::vector<float>& values);
float compute_standard_deviation(const std::vector<int>& values, const float& mean);
/// Normal probability density function
template <typename T>
T compute_normal_pdf(T x, T m, T s)
{
	static const T inv_sqrt_2pi = 0.3989422804014327;
	T a = (x - m) / s;

	return inv_sqrt_2pi / s * std::exp(-static_cast<T>(0.5) * a * a);
}

/// Custom exception used for the read_argument function
class NotImplementedException : public std::logic_error {
	NotImplementedException() = delete;
public:
	virtual char const* what() const noexcept override { return "Function not implemented"; }
};

class Chrono {
public:
    ~Chrono() = default;

    inline void start() { m_start = cv::getTickCount(); }
    inline void stop() { m_stop = cv::getTickCount(); }
    inline double elapsed_time() const { return (m_stop - m_start) / cv::getTickFrequency(); }

private:
    int64 m_start;
    int64 m_stop;
};

struct Gaussian {
    float m_weight;
    float m_mu;
    float m_sigma;

    Gaussian(const float& weight, const float& mu, const float& sigma) :m_weight(weight), m_mu(mu), m_sigma(sigma) {}
    void update_mu_sigma(const float& value, const float& alpha);
    void update_weight(const float& alpha, const bool& matched);
};

std::ostream& operator<<(std::ostream& os, const Gaussian& gaussian);