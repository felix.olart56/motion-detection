#pragma once

#ifdef _WIN32
#pragma warning(push, 0)
#endif
#include <opencv2/opencv.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <nlohmann/json.hpp>
#ifdef _WIN32
#pragma warning(pop)
#endif

#include "Utils.hpp"

using json = nlohmann::json;

class CameraManager {
public:
    inline ~CameraManager() { m_video.release(); }

    bool initialize(const int& index, const std::string& file_path, const int& api_preference = cv::CAP_ANY);
    void manage_events(const int& key_pressed);

    bool set_video(const int& index, const int& api_preference);
    inline cv::VideoCapture& get_video() noexcept { return m_video; }

    void print_settings() const;

private:
    cv::VideoCapture m_video;
    std::vector<CameraSetting> m_settings;

    void setup_settings(json file);
};