#pragma once

#include "VideoManager.hpp"
#include "CameraManager.hpp"
#include "Utils.hpp"

class App {
public:
	virtual ~App() noexcept {};

	virtual void initialize(const std::map<std::string, std::pair<bool, std::string>>& args) = 0;
	virtual void run() = 0;
	virtual void close() = 0;

	inline CameraManager& get_camera() { return m_camera; }
	inline VideoManager& get_movie() { return m_movie; }
	cv::VideoCapture get_video();

	inline void set_run(const bool& new_state) { m_run = new_state; }
	inline bool get_run() const { return m_run; }

	inline bool get_is_movie() { return m_is_movie; }

	void read_frame(cv::Mat& frame);
	std::vector<std::string> get_options();

	bool initialize_outputs(const std::string file_name, const int& fps, const bool& color = false, const char fcc[4] = "mp4v");
	inline void write_outputs(const cv::Mat& frame, const cv::Mat& foreground) { m_output.write(frame); m_output_foreground.write(foreground); }
	inline void release_outputs() { m_output.release(); m_output_foreground.release(); }

	enum Key {
		ZERO = 48, ONE = 49, TWO = 50, THREE = 51, FOUR = 52, FIVE = 53, SIX = 54, SEVEN = 55, HEIGHT = 56, NINE = 57,
		ESC = 27,
		//LEFT = 37, UP = 38, RIGHT = 39, DOWN = 40, not working
		A = 97, B = 98, C = 99, D = 100, E = 101, R = 114, S = 115, T = 116, Z = 122
	};

	enum class Arg { OPTIONAL, REQUIRED };

	enum class ArgType { INT, FLOAT, BOOL, STRING };

protected:
	template<typename T>
	T read_argument(const std::map<std::string, std::pair<bool, std::string>>& args, const std::string& option);

	std::string m_name;
	bool m_run = true;
	bool m_is_movie = false;

	CameraManager m_camera;
	VideoManager m_movie;
	cv::VideoWriter m_output;
	cv::VideoWriter m_output_foreground;
	// Data structure is as follows: {option, {arg, {type, default_value}}}
	// Example: {"-f", {OPTIONAL, {STRING, "my_default_string"}}}
	virtual const std::map<std::string, std::pair<App::Arg, std::pair<App::ArgType, std::string>>> get_args() const = 0;
};