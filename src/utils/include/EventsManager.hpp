#pragma once

#include "App.hpp"

class EventsManager {
public:
    virtual ~EventsManager() noexcept {};

    virtual void manage_events(App& app, const int& key_pressed) = 0;
};