#pragma once

#include "../utils/include/EventsManager.hpp"

class BasicModelEvents : public EventsManager {
public:
	~BasicModelEvents() {}

	void manage_events(App& app, const int& key_pressed) override;

private:

};