set(OUTPUT_NAME "basic_model")

file(GLOB_RECURSE HEADER_FILES *.hpp)
file(GLOB_RECURSE SRC_FILES *.cpp)
file(COPY settings DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

add_executable(${OUTPUT_NAME} ${SRC_FILES} ${UTILS_SRC_FILES} ${HEADER_FILES} ${UTILS_HEADER_FILES})
target_link_libraries(${OUTPUT_NAME} ${OpenCV_LIBS} nlohmann_json::nlohmann_json)
if(UNIX)
    target_compile_options(${OUTPUT_NAME} PRIVATE -W -Wall -Werror -O2)
elseif(WIN32)
    target_compile_options(${OUTPUT_NAME} PRIVATE -Wall)
endif()