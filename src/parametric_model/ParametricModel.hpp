#pragma once

#include "../utils/include/App.hpp"
#include "ParametricModelEvents.hpp"

class ParametricModel : public App {
public:
	ParametricModel() {};
	~ParametricModel() noexcept {};

	void initialize(const std::map<std::string, std::pair<bool, std::string>>& args) override;
	void run() override;
	void close() override;

	// Singleton setup
	ParametricModel(ParametricModel& other) = delete;
	void operator=(ParametricModel& singleton) = delete;
	inline static ParametricModel& get() noexcept { return m_singleton; }

	const std::map<std::string, std::pair<App::Arg, std::pair<App::ArgType, std::string>>> get_args() const override { return this->m_args;	}

	// Model methods
	inline float get_threshold() const { return m_threshold; }
	inline void set_threshold(const float& new_threshold) { m_threshold = new_threshold; }
	inline bool get_use_gmm() const { return m_use_gmm; }
	inline void set_use_gmm(const bool& new_state) { m_use_gmm = new_state; }
	
private:
	static ParametricModel m_singleton;
	ParametricModelEvents m_events;

	// Model methods
	void compute_background_model();
	cv::Mat compute_foreground(const cv::Mat& frame, const float& threshold) const;
	void compute_gmm_background_model(const uint& max_gaussians, const float& alpha);
	cv::Mat compute_gmm_foreground(const cv::Mat& frame, const float& max_gaussians, const float& threshold) const;

	std::vector<size_t> argmin_gmm(const std::vector<Gaussian*>& gaussians, const float& threshold) const;
	inline bool match_gmm(const uint& value, const Gaussian* g) const { return ((std::abs(value - g->m_mu)) / g->m_sigma) <= 2.5f; }

	// Model attributes
	std::vector<cv::Mat> m_tmp_frames;
	cv::Mat3f m_background;
	std::vector<Gaussian*> m_gmm_background;

	// Argument attributes
	int m_camera_index = 0;
	int m_camera_api_preference = cv::CAP_ANY; // cv::CAP_V4L = 200
	std::string m_camera_settings = "settings/UBUNTU_CameraSettings.json";
	std::string m_video_path = "";
	uint m_nb_frames_compute_background = 100;
	bool m_use_gmm = false;
	float m_threshold = 0.0f;
	int m_max_gaussians = 3;
	float m_alpha = 0.01f;
	float m_init_sigma = 10.0f;
	bool m_save = false;
	std::string m_output_name = "";
	const std::map<std::string, std::pair<App::Arg, std::pair<App::ArgType, std::string>>> m_args = {
		{"-cam_idx", {App::Arg::OPTIONAL, {App::ArgType::INT, "0"}}},										// Camera index
		{"-cam_api", {App::Arg::OPTIONAL, {App::ArgType::INT, "0"}}},										// Camera api preference
		{"-cam_set", {App::Arg::OPTIONAL, {App::ArgType::STRING, "settings/UBUNTU_CameraSettings.json"}}},	// Camera settings
		{"-video", {App::Arg::OPTIONAL, {App::ArgType::STRING, ""}}},										// Input file path
		{"-bg", {App::Arg::OPTIONAL, {App::ArgType::INT, "100"}}},											// Number of frames to compute the background model
		{"-gmm", {App::Arg::OPTIONAL, {App::ArgType::BOOL, ""}}},											// Whether to use GMM background model or not
		{"-mg", {App::Arg::OPTIONAL, {App::ArgType::INT, "3"}}},											// Number of maximum gaussians to use when using the GMM backgroud model
		{"-t", {App::Arg::REQUIRED, {App::ArgType::FLOAT, ""}}},											// Threshold to use
		{"-a", {App::Arg::OPTIONAL, {App::ArgType::FLOAT, "0.01"}}},										// Learning rate for the GMM background modal
		{"-is", {App::Arg::OPTIONAL, {App::ArgType::FLOAT, "10.0"}}},										// Value used to initialize gaussians in the GMM background
		{"-save", {App::Arg::OPTIONAL, {App::ArgType::BOOL, ""}}},											// Whether to save generated images in a video
		{"-o", {App::Arg::OPTIONAL, {App::ArgType::STRING, ""}}},											// Output name
	};
};