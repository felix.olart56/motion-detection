#pragma once

#include "../utils/include/EventsManager.hpp"

class ParametricModelEvents : public EventsManager {
public:
	~ParametricModelEvents() {}

	void manage_events(App& app, const int& key_pressed) override;

private:

};