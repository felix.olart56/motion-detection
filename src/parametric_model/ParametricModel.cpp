#include "ParametricModel.hpp"


/// Initialize the model
void ParametricModel::initialize(const std::map<std::string, std::pair<bool, std::string>>& args) {
	// Read arguments
	try {
		m_camera_index = read_argument<int>(args, "-cam_idx");
		m_camera_api_preference = read_argument<int>(args, "-cam_api");
		m_camera_settings = read_argument<std::string>(args, "-cam_set");
		m_video_path = read_argument<std::string>(args, "-video");
		m_nb_frames_compute_background = read_argument<int>(args, "-bg");
		m_use_gmm = read_argument<bool>(args, "-gmm");
		m_threshold = read_argument<float>(args, "-t");
		m_max_gaussians = read_argument<int>(args, "-mg");
		m_alpha = read_argument<float>(args, "-a");
		m_init_sigma = read_argument<float>(args, "-is");
		m_save = read_argument<bool>(args, "-save");
		m_output_name = read_argument<std::string>(args, "-o");
	}
	catch (const std::exception& exception) {
		std::cerr << "An error occured reading the list of options..." << std::endl;
		std::cerr << exception.what() << std::endl;
		m_run = false;
		return;
	}

	// Initialize model attributes
	m_name = "ParametricModel";
	
	// Initialize the video manager or the camera manager
	bool initialized;
	if (m_video_path.empty()) {
		initialized = m_camera.initialize(m_camera_index, m_camera_settings, m_camera_api_preference);
	}
	else {
		initialized = m_movie.initialize(m_video_path);
		m_is_movie = true;
		// Check that the video contains enough frames to compute the backgound model
		if (m_nb_frames_compute_background >= m_movie.get_length()) {
			std::cerr << "You can't select more frames than the given video contains to compute a background model..." << std::endl;
			m_run = false;
			return;
		}
	}
	if (!initialized) { m_run = false; }

	// Initialize output videos
	if (m_save) {
		if (!initialize_outputs(m_output_name == "" ? m_name : m_output_name, 24)) {
			std::cerr << "Could not open the output videos for write." << std::endl;
			m_run = false;
			return;
		}
	}
}


/// Main function of the model.
void ParametricModel::run() {
	// Exit before doing anything if something went wrong during initialization
	if (!m_run) { return; }

	uint frame_id;
	Chrono chrono;
	cv::Mat frame, foreground;
	cv::Mat kernel(8, 8, CV_8U, cv::Scalar(1, 1, 1));

	uint nb_frames = (m_is_movie) ? m_movie.get_length() : std::numeric_limits<uint>::max();
	int delay = (m_is_movie) ? std::ceil<int>(1000.0f / static_cast<float>(m_movie.get_fps())) : 1;

	// Get images to compute the background model
	for (frame_id = 0; frame_id < m_nb_frames_compute_background + (m_is_movie ? 0 : 10); ++frame_id) {
		if (!m_run) { break; }
		read_frame(frame);

		// Let ten or so frames to let the camera setting everything up (when using a webcam)
		if (m_is_movie || frame_id > 9) {
			cv::cvtColor(frame, frame, cv::COLOR_RGB2GRAY);

			m_tmp_frames.push_back(frame.clone());
		}

		cv::imshow("Live view", frame);
		m_events.manage_events(*this, cv::waitKey(delay));
	}

	if (m_run) {
		// Compute background model
		if (!m_use_gmm) {
			chrono.start();
			compute_background_model();
			chrono.stop();
			std::cout << "Compute background model: " << chrono.elapsed_time() << "s" << std::endl;
		}
		else {
			chrono.start();
			compute_gmm_background_model(m_max_gaussians, m_alpha);
			chrono.stop();
			std::cout << "Compute GMM background model: " << chrono.elapsed_time() << "s" << std::endl;
		}

		// Release memory used to compute the model
		m_tmp_frames.clear();

		// Play the video from the beginning
		m_movie.get_video().set(cv::CAP_PROP_POS_FRAMES, 0);
	}

	double cumulated_time = 0.0;
	for (frame_id = 0; frame_id < nb_frames; ++frame_id) {
		if (!m_run) { break; }
		read_frame(frame);
		cv::cvtColor(frame, frame, cv::COLOR_RGB2GRAY);

		if (!m_use_gmm) {
			chrono.start();
			foreground = compute_foreground(frame, m_threshold);
			cv::morphologyEx(foreground, foreground, cv::MORPH_OPEN, kernel); // Remove part of noise
			chrono.stop();
			cumulated_time += chrono.elapsed_time();
			cv::imshow(m_name, foreground);
			cv::imshow("Background model", m_background);
		}
		else {
			chrono.start();
			foreground = compute_gmm_foreground(frame, m_max_gaussians, m_threshold);
			cv::morphologyEx(foreground, foreground, cv::MORPH_OPEN, kernel); // Remove part of noise
			chrono.stop();
			cumulated_time += chrono.elapsed_time();
			cv::imshow(m_name, foreground);
		}

		cv::imshow("Live view", frame);
		if (m_save) {
			write_outputs(frame, foreground);
		}
		m_events.manage_events(*this, cv::waitKey(delay));
	}

	if (frame_id > 0) {
		std::cout << "Compute foreground mean time per frame: " << cumulated_time / static_cast<double>(frame_id + 1) << "s" << std::endl;
	}
}


/// Function called when closing / stopping the programm.
void ParametricModel::close() {
	// Delete gaussians in the background model
	for (auto gaussian : m_gmm_background) {
		delete gaussian;
	}
	if (m_save) {
		release_outputs();
	}
	cv::destroyAllWindows();
}


/// Naive method to compute a background model (Simple Gaussian)
void ParametricModel::compute_background_model() {
	const uint width = static_cast<uint>(m_tmp_frames[0].rows);
	const uint height = static_cast<uint>(m_tmp_frames[0].cols);

	cv::Mat background = cv::Mat::zeros(cv::Size(height, width), CV_32FC3);
	float mean = 0.0f;

	std::vector<int> values;
	for (uint pixel_id = 0; pixel_id < width * height; ++pixel_id) {
		for (auto& frame : m_tmp_frames) {
			values.push_back(frame.ptr()[pixel_id]);
		}
		mean = compute_mean(values);
		background.at<cv::Vec3f>(pixel_id)[0] = mean / 255.0f;
		background.at<cv::Vec3f>(pixel_id)[1] = compute_standard_deviation(values, mean) / 255.0f;

		values.clear();
	}

	m_background = background;
}


/// Compute the foreground using the computed background model and the given threshold
cv::Mat ParametricModel::compute_foreground(const cv::Mat& frame, const float& threshold) const {
	const uint width = static_cast<uint>(frame.rows);
	const uint height = static_cast<uint>(frame.cols);

	cv::Mat foreground = cv::Mat::zeros(cv::Size(height, width), CV_8U);
	float g_value = 0.0f;

	for (uint pixel_id = 0; pixel_id < width * height; ++pixel_id) {
		g_value = compute_normal_pdf<float>(frame.ptr()[pixel_id], m_background.at<cv::Vec3f>(pixel_id)[0] * 255, m_background.at<cv::Vec3f>(pixel_id)[1] * 255);
		if (g_value < threshold) {
			foreground.ptr()[pixel_id] = 255;
		}
		else {
			foreground.ptr()[pixel_id] = 0;
		}
	}

	return foreground;
}


/// Compute a background model using the GMM method (Gaussian Mixture Model)
void ParametricModel::compute_gmm_background_model(const uint& max_gaussians, const float& alpha = 0.01f) {
	const uint width = static_cast<uint>(m_tmp_frames[0].rows);
	const uint height = static_cast<uint>(m_tmp_frames[0].cols);
	size_t frame_id = 0;
	uint gaussian_id = 0;

	std::vector<Gaussian*> background;

	const std::vector<float> init_weights = { 0.7f, 0.1f };
	std::vector<Gaussian*> tmp_gaussians;
	bool matched_gaussians = false;
	float weight;
	bool matched;

	auto sum_weights = [](float acc, const Gaussian* g) -> float {
		return acc + g->m_weight;
	};
	float sum;

	for (uint pixel_id = 0; pixel_id < width * height; ++pixel_id) {
		
		for (frame_id = 0; frame_id < m_tmp_frames.size(); ++frame_id) {

			// Initialization: create a Gaussian to model the first observation
			if (frame_id == 0) {
				tmp_gaussians.push_back(new Gaussian(1.0f, m_tmp_frames[frame_id].ptr()[pixel_id], m_init_sigma));
				continue;
			}

			// Iterative step: check if any Gaussians will generate current observation
			for (auto gaussian : tmp_gaussians) {
				// Look for Gaussians which could match
				// If Gaussian matched with the current observation, update it
				matched = match_gmm(m_tmp_frames[frame_id].ptr()[pixel_id], gaussian);
				gaussian->update_weight(alpha, matched);
				if (matched) {
					gaussian->update_mu_sigma(m_tmp_frames[frame_id].ptr()[pixel_id], alpha);
					matched_gaussians = true;
				}
			}
			
			// Else, if no Guassian matched, create one to model the observation
			if (!matched_gaussians) {
				// Check if the max number of Gaussians is not already reached
				// If max number reached, delete the Gaussian which has the smallest weight
				if (tmp_gaussians.size() >= max_gaussians) {
					std::sort(tmp_gaussians.begin(), tmp_gaussians.end(), [](const Gaussian* g_a, const Gaussian* g_b) -> bool {
						return g_a->m_weight < g_b->m_weight;
					});
					delete *tmp_gaussians.begin();
					tmp_gaussians.erase(tmp_gaussians.begin());
				}

				++gaussian_id;
				weight = init_weights[gaussian_id >= init_weights.size() ? init_weights[init_weights.size() - 1] : init_weights[gaussian_id]];
				tmp_gaussians.push_back(new Gaussian(weight, m_tmp_frames[frame_id].ptr()[pixel_id], m_init_sigma));
			}

			matched_gaussians = false;
		}

		// Normalize weights to make sure the sum of all weights is equal to one
		sum = std::accumulate(tmp_gaussians.begin(), tmp_gaussians.end(), 0.0f, sum_weights);
		std::for_each(tmp_gaussians.begin(), tmp_gaussians.end(), [sum](Gaussian* g) -> void { g->m_weight /= sum; });

		// Sort Gaussians in a descending order with respect to weight/sigma
		std::sort(tmp_gaussians.begin(), tmp_gaussians.end(), [](const Gaussian* g_a, const Gaussian* g_b) -> bool {
			return (g_a->m_weight / g_a->m_sigma) > (g_b->m_weight / g_b->m_sigma);
		});

		// Insert Gaussians for the current pixel
		background.insert(background.end(), tmp_gaussians.begin(), tmp_gaussians.end());
		// Insert null pointers if the max number of Gaussians is not reached
		if (tmp_gaussians.size() < max_gaussians) {
			std::vector<Gaussian*> null_pointers(max_gaussians - tmp_gaussians.size(), nullptr);
			background.insert(background.end(), null_pointers.begin(), null_pointers.end());
		}

		gaussian_id = 0;
		tmp_gaussians.clear();
	}

	m_gmm_background = background;
}


/// Compute a the foreground using the GMM background model
cv::Mat ParametricModel::compute_gmm_foreground(const cv::Mat& frame, const float& max_gaussians, const float& threshold) const {
	const uint width = static_cast<uint>(frame.rows);
	const uint height = static_cast<uint>(frame.cols);

	cv::Mat foreground = cv::Mat::zeros(cv::Size(height, width), CV_8U);

	std::vector<Gaussian*> gaussians;
	std::vector<size_t> bg_gaussians_indices;
	std::vector<Gaussian*>::const_iterator gaussian_it;

	for (uint pixel_id = 0; pixel_id < width * height; ++pixel_id) {

		gaussian_it = m_gmm_background.begin() + max_gaussians * pixel_id;
		gaussians.insert(gaussians.end(), gaussian_it, gaussian_it + max_gaussians);

		// Background selection
		bg_gaussians_indices = argmin_gmm(gaussians, threshold);
		for (auto& index : bg_gaussians_indices) {
			if (!match_gmm(frame.ptr()[pixel_id], gaussians[index])) {
				foreground.ptr()[pixel_id] = 255;
			}
		}

		gaussians.clear();
	}

	return foreground;
}


/// Custom argmin function
std::vector<size_t> ParametricModel::argmin_gmm(const std::vector<Gaussian*>& gaussians, const float& threshold) const {
	float sum = 0.0f;
	std::vector<size_t> bg_indices;

	// For each gaussian id
	for (size_t gaussian_id = 0; gaussian_id < gaussians.size(); ++gaussian_id) {

		// Compute the sum of all the weights
		for (size_t id = 0; id < gaussian_id + 1; ++id) {
			if (gaussians[id] != nullptr) {
				sum += gaussians[id]->m_weight;
			}

			bg_indices.push_back(id);
		}

		// If the sum is greater than the threshold, then these indices are to be considered as the background
		if (sum >= threshold) {
			return bg_indices;
		}

		bg_indices.clear();
		sum = 0.0f;
	}

	// Else, return an epmty list
	return std::vector<size_t>();
}