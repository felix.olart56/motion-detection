#include "ParametricModel.hpp"

ParametricModel ParametricModel::m_singleton;

int main(int argc, char* argv[]) {
	ParametricModel& model = ParametricModel::get();
	auto arguments = parse_command_line_args(argc, argv, model.get_options());

	model.initialize(arguments);
	model.run();
	model.close();

	return 0;
}